Name: almalinux-indexhtml
Version: 8
Release: 9%{?dist}
Summary: Browser default start page for AlmaLinux
Source: index.html
License: Distributable
Group: Documentation
BuildArch: noarch

Obsoletes: redhat-indexhtml
Obsoletes: centos-indexhtml

Provides: redhat-indexhtml = %{version}-%{release}
Provides: centos-indexhtml = %{version}-%{release}

%description
The indexhtml package contains the welcome page shown by your Web browser,
which you'll see after you've successfully installed AlmaLinux.

The Web page provided by indexhtml tells you how to register your AlmaLinux
software and how to get any support that you might need.

%prep
%setup -q -c -T

%build

%install
mkdir -p $RPM_BUILD_ROOT/%{_defaultdocdir}/HTML/en-US/
install -m644 %{SOURCE0} $RPM_BUILD_ROOT/%{_defaultdocdir}/HTML/en-US/index.html
ln -sf en-US/index.html $RPM_BUILD_ROOT/%{_defaultdocdir}/HTML/index.html


%files
%{_defaultdocdir}/HTML/*

%changelog
* Thu Nov 21 2024 Sofia Boldyreva <sboldyreva@almalinux.org> - 8-9
- Update index.html to fix link on the logo.

* Wed Nov 06 2024 Sofia Boldyreva <sboldyreva@almalinux.org> - 8-8
- Added updated index.html to sources.
- Updated prep and install sections.

* Tue Jun 08 2021 Andrei Lukoshko <alukoshko@almalinux.org> - 8-7.1
- Update tarball

* Wed Jan 20 2021 Anatholy Scryabin <ascryabin@cloudlinux.com> - 8-7
- Initial build for AlmaLinux
